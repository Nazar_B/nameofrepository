<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 24.04.18
 */
//
//class BookController extends Controller
//{
//    public function createAction(Request $request)
//    {
//        return $this->render('create');
//    }
//
//
//    public function storeAction(Request $request)
//    {
//        if (!$request->isPost()) {
//            return false;
//        }
//
//        $db = DbConnection::getInstance();
//
//        $title = $request->post('Name');
//        $author = $request->post('Author');
//        $year = $request->post('Data');
//
//        $db->getMysqli()
//            ->query("INSERT INTO `libary\` (`Name`, `Author`, `Data`) VALUES ('$title', '$year', '$author')");
//
//        $_SESSION['status'] = 'Book ' . $title . ' has been successfully stored';
//
//        return header('Location: ?route=book/index');
//    }
//
//    public function indexAction()
//    {
//        $db = DbConnection::getInstance();
//
//        $books = $db->getMysqli()->query("SELECT * from libary/");
//
//        $status = isset($_SESSION['status']) ? $_SESSION['status'] : null;
//
//        unset($_SESSION['status']);
//
//        return $this->render('index', [
//            'books' => $books,
//            'status' => $status
//        ]);
//    }
//}


// ?route=book/create
// просто рисует вьюхи
// title, author, year - поля формы, которая при сабмите отправляется на обработчик


// ?route=book/store $_SESSION['status'] = Book has been created successfully
// ложит данные книги в базу, устанавливает значение сессии.


// ?route=book/index
// в экшине контроллера проверяет сессию со статусом, если в наличии - удаляет.
// отрисовывает вьюху со списком книг