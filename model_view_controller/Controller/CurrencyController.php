<?php
class CurrencyController extends Controller
{
    const API = "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5";
    public function getJson()
    {
        $json = file_get_contents(self::API);
        $json = json_decode($json);
        return $json;
    }
    public function indexAction()
    {
        foreach ($this->getJson() as $item) {
            if ($item->ccy === "USD") {
                $buyUSD = $item->buy;
                $sellUSD = $item->sale;
            }
            if ($item->ccy === "EUR") {
                $buyEUR = $item->buy;
                $sellEUR = $item->sale;
            }
            if ($item->ccy === "BTC") {
                $buyBTC = $item->buy;
                $sellBTC = $item->sale;
            }
            if ($item->ccy === "RUR") {
                $buyRUR = $item->buy;
                $sellRUR = $item->sale;
            }
        }
            return $this->render('index',[
                'BuyUSD' => $buyUSD,
                'SellUSD' => $sellUSD,
                'BuyEUR' => $buyEUR,
                'SellEUR' => $sellEUR,
                'BuyBTC' => $buyBTC,
                'SellBTC' => $sellBTC,
                'BuyRUR' => $buyRUR,
                'SellRUR' => $sellRUR
            ]);
        }
    public function calculateAction(Request $request) {
        if (!$request->isPost()) {
            return false;
        }

        foreach ($this->getJson() as $item) {
            if ($item->ccy === "USD") {
                $buyUSD = $item->buy;
                $sellUSD = $item->sale;
            }
            if ($item->ccy === "EUR") {
                $buyEUR = $item->buy;
                $sellEUR = $item->sale;
            }
            if ($item->ccy === "BTC") {
                $buyBTC = $item->buy;
                $sellBTC = $item->sale;
            }
            if ($item->ccy === "RUR") {
                $buyRUR = $item->buy;
                $sellRUR = $item->sale;
            }
        }
        return header('Location: ?route=currency/converter');
    }
}