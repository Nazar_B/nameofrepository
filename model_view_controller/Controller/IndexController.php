<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 23.04.18
 */

class IndexController extends Controller
{
    public function indexAction(Request $request)
    {
        $db = DbConnection::getInstance();
        var_dump($db);
        if (isset($_SESSION['name'])) {
            $userName = $_SESSION['name'];
        } else {
            $userName = $request->get('name');
            $_SESSION['name'] = $userName;
        }

        $result = pow($request->get('number'), $request->get('exp'));

        return $this->render('index', ['result1' => $result, 'name' => $userName]);
    }
}